import pygame as pg
import numpy as np






#colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)



class World:

    def __init__(self):
        self.init_pygame()
        self.init_world_objects()

        #last command
        self.world_loop()

    def init_world_objects(self):
        move_dist = 5
        self.CAR_MOVES = {pg.K_LEFT : (-move_dist, 0),
                     pg.K_RIGHT : (move_dist, 0),
                     pg.K_UP : (0, -move_dist),
                     pg.K_DOWN : (0, move_dist)
                    }
        self.car_controls = {pg.K_LEFT : False,
                        pg.K_RIGHT : False,
                        pg.K_UP : False,
                        pg.K_DOWN : False
                       }

        self.car_img = pg.image.load('snake.png')

        self.car_pos = np.array([self.display_width * 0.4, self.display_height * 0.8])



    def init_pygame(self):

        pg.init()

        self.display_width = 800
        self.display_height = 600

        self.display_size = (self.display_width, self.display_height)
        self.world_display = pg.display.set_mode(self.display_size)

        pg.display.set_caption('A bit Racey')

        self.clock = pg.time.Clock()
        self.background_color = WHITE

        self.in_play = True

    def choose_action(self, event):
        if event.type == pg.QUIT:
            self.in_play = False
        if event.type == pg.KEYDOWN:
            if event.key in self.car_controls:
                self.car_controls[event.key] = True
        if event.type == pg.KEYUP:
            if event.key in self.car_controls:
                self.car_controls[event.key] = False

    def update_world(self):
        self.update_car_position()

    def update_car_position(self):
        for key in self.car_controls:
            if self.car_controls[key]:
                self.move_car(self.CAR_MOVES[key])

    def move_car(self, delta):
        self.car_pos += delta

    def redraw_world(self):
        self.world_display.fill(self.background_color)

        self.world_display.blit(self.car_img, tuple(self.car_pos))
        #or flip
        #update() updates all, with params updates only it
        pg.display.update()
        #fps if want fast but smooth movement make over 30
        self.clock.tick(60)

    def world_loop(self):
        while self.in_play:
            #list of events per frame
            for event in pg.event.get():
                self.choose_action(event)
            self.update_world()
            self.redraw_world()
        self.quit()

    def quit(self):
        pg.quit()
        quit()


world = World()
